'use strict';

const path = require('path');
const autoprefixer = require('autoprefixer');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
    entry: './src/js/app.js',
    performance: {
        hints: false,
    },
    output: {
        filename: 'assets/js/app.[contenthash].bundle.js',
        path: path.resolve(__dirname, 'public')
    },
    mode: 'production',
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.(scss)$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                plugins: [autoprefixer]
                            }
                        }
                    },
                    {
                        loader: 'sass-loader'
                    }
                ]
            },
            {
                test: /\.(css)$/i,
                use: ['style-loader', 'css-loader']
            },
            // Exclude .scss files from being treated as assets/resources
            {
                test: /\.(png|jpe?g|gif|svg)$/i,
                type: 'asset/resource',
                generator: {
                    filename: 'assets/images/[name][ext]'
                },
                exclude: /\.scss$/,
            },
            {
                test: /\.webp$/i,
                type: 'asset',
                generator: {
                    filename: 'assets/images/[name][ext]'
                },
                exclude: /\.scss$/,
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/i,
                type: 'asset/resource',
                generator: {
                    filename: 'assets/fonts/[name][ext]'
                },
                exclude: /\.scss$/,
            }
        ]
    },
    devServer: {
        static: {
            directory: path.join(__dirname, 'public'),
        },
        compress: true,
        port: 7777,
        hot: true,
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'src', 'index.html'),
            filename: 'index.html',
            inject: 'body',
            minify: false
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'src', 'contacto.html'),
            filename: 'contacto.html',
            inject: 'body',
            minify: false
        }),
        new CopyPlugin({
            patterns: [
                {
                    from: path.resolve(__dirname, 'src'),
                    to: path.resolve(__dirname, 'public'),
                    globOptions: {
                        ignore: ['**/*.html'] // Exclude HTML files
                    }
                },
                {
                    from: path.resolve(__dirname, 'src', 'assets'),
                    to: path.resolve(__dirname, 'public', 'assets'),
                    globOptions: {
                        ignore: ['**/scss/**'] // Exclude the scss folder
                    }
                }
            ],
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ]
};