// core version + navigation, pagination modules:
import Swiper from 'swiper';
import { Autoplay, Navigation, Pagination } from 'swiper/modules';
// import Swiper and modules styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';

// init Swipers:
const swiperJumbotron = new Swiper('.swiper-jumbotron', {
    // configure Swiper to use modules
    modules: [Navigation, Pagination],

    // Pagination
    pagination: {
        el: ".swiper-pagination",
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});

const swiperClientes = new Swiper('.swiper-clientes', {
  // configure Swiper to use modules
  modules: [Autoplay],

  speed: 4500,
  centeredSlides: true,
  spaceBetween: 30,
  slidesPerView: 2,
  loop: true,
  autoplay: {
      delay: 1,
      disableOnInteraction: false,
      reverseDirection: false,
  },
  noSwiping: true,
  noSwipingClass: 'no-swiping',
  breakpoints: {
      576: {
        slidesPerView: 2,
      },
      768: {
        slidesPerView: 4,
      },
      992: {
        slidesPerView: 6,
      },
      1200: {
          slidesPerView: 8,
      },
  },
});

const swiperClientes2 = new Swiper('.swiper-clientes-2', {
  // configure Swiper to use modules
  modules: [Autoplay],

  speed: 4500,
  centeredSlides: true,
  spaceBetween: 30,
  slidesPerView: 2,
  loop: true,
  autoplay: {
      delay: 1,
      disableOnInteraction: false,
      reverseDirection: false,
  },
  noSwiping: true,
  noSwipingClass: 'no-swiping',
  breakpoints: {
      576: {
        slidesPerView: 2,
      },
      768: {
        slidesPerView: 4,
      },
      992: {
        slidesPerView: 6,
      },
      1200: {
          slidesPerView: 8,
      },
  },
});