// Import our custom CSS
import '../scss/main.scss';

// Import all of Bootstrap's JS
import * as bootstrap from 'bootstrap';

// Local Scripts
import './smooth-scrolling';
import './form-ajax';
import './aos';
import './swipers';

// Header
document.addEventListener('DOMContentLoaded', function () {
    // Cache the DOM element containing the navbar
    var header = document.getElementById('navbar');

    function updateScroll() {
        var scroll = window.pageYOffset || document.documentElement.scrollTop;

        if (scroll >= 1) {
            header.classList.add('navbar-scroll');
        } else {
            header.classList.remove('navbar-scroll');
        }
    }

    window.addEventListener('scroll', updateScroll);
    updateScroll();
});

// Navigation menu
document.getElementById('mburger').addEventListener('click', function (e) {
    e.stopPropagation();
    var menu = document.querySelector('.menu');
    var navbar = document.getElementById('navbar');
    var backdrop = document.getElementById('backdrop');

    menu.classList.toggle('menu-abierto');
    navbar.classList.toggle('opacity-0');
    backdrop.classList.toggle('backdrop-opacity-1');
});

document.querySelector('.menu').addEventListener('click', function (e) {
    e.stopPropagation();
});

document.body.addEventListener('click', function (e) {
    var menu = document.querySelector('.menu');
    var navbar = document.getElementById('navbar');
    var backdrop = document.getElementById('backdrop');

    menu.classList.remove('menu-abierto');
    navbar.classList.remove('opacity-0');
    backdrop.classList.remove('backdrop-opacity-1');
});

document.getElementById("cerrar-menu").addEventListener("click", cerrarMenu, false);
document.getElementById("btn-logo").addEventListener("click", cerrarMenu, false);

// Get the ul element by its ID
var ulElement = document.getElementById("navmenu");

// Get all li elements within the ul element
var liElements = ulElement.getElementsByTagName("li");

// Loop through each li element
for (var i = 0; i < liElements.length; i++) {
    // Do stuff with each li element
    var currentLi = liElements[i];
    currentLi.addEventListener("click", function() {
        var menu = document.querySelector('.menu');
        var navbar = document.getElementById('navbar');
        var backdrop = document.getElementById('backdrop');
        
        menu.classList.remove('menu-abierto');
        navbar.classList.remove('opacity-0');
        backdrop.classList.remove('backdrop-opacity-1');
    });
}

document.getElementById("btn-contacto").addEventListener("click", cerrarMenu, false);

function cerrarMenu() {
    $('.menu').removeClass('menu-abierto');
    $('#navbar').removeClass('opacity-0');
    $('#backdrop').removeClass('backdrop-opacity-1');
}

// Cerrar menú con Esc
document.addEventListener('keydown', (event) => {
    if (event.key === 'Escape') {
        cerrarMenu();
    }
});

// Masonry-Tabs bug fix
import Masonry from 'masonry-layout'; // Import Masonry library

const tabEls = document.querySelectorAll('button[data-bs-toggle="pill"]');
const masonryGrids = document.querySelectorAll('.masonry-grid');

tabEls.forEach(tabEl => {
    tabEl.addEventListener('shown.bs.tab', event => {
        // Get the ID of the tab content being shown
        const targetId = event.target.getAttribute('data-bs-target');
        const tabContent = document.querySelector(targetId);
        
        // Find the Masonry grid within the tab content
        const masonryGrid = tabContent.querySelector('.masonry-grid');
        
        // If Masonry grid found, initialize or reload it
        if (masonryGrid) {
            // Destroy the existing Masonry instance if it exists
            if (masonryGrid.masonry) {
                masonryGrid.masonry.destroy();
            }
            
            // Reinitialize Masonry
            new Masonry(masonryGrid, {
                // Your Masonry options here
                itemSelector: '.masonry-item', // Assuming your Masonry items have a class of 'masonry-item'
                // other options...
            });
        }
    });
});
